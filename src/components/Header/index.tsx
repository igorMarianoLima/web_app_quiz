import React, { useContext } from 'react';
import QuestionsContext from '../../context/questions';
import { Container, Navigator } from './styles';

const Header: React.FC = () => {
   const {actualQuestion, questions} = useContext(QuestionsContext);

   return(
      <Container>
         <Navigator>
            <span>{actualQuestion}/{questions.length}</span>
         </Navigator>
      </Container>
   )
}

export default Header;