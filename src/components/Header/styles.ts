import styled from "styled-components";

export const Container = styled.header`
   width: 100%;

   padding: 20px;

   background-color: #fff;
   box-shadow: 0px 2px 2px #3c55fa1a;
`;

export const Navigator = styled.nav`
   width: 100%;
   
   display: flex;
   justify-content: space-between;
   align-items: center;
`;