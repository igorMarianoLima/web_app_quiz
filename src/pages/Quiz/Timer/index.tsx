import React from "react";
import { ProgressBar } from "./styles";

interface TimerProps {
   secondsElapsed: number;
   maxTimeInSeconds: number;
}

const Timer: React.FC<TimerProps> = ({maxTimeInSeconds, secondsElapsed}) => {
   return(
      <ProgressBar
         max={maxTimeInSeconds}
         value={maxTimeInSeconds - secondsElapsed}
         percentage={100 - (secondsElapsed * 100) / maxTimeInSeconds || 100}
      />
   )
}

export default Timer;