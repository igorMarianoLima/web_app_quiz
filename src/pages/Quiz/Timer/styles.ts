import styled from "styled-components";
import colors from '../../../styles/colors';

interface ProgressBarProps {
   percentage: number;
}

export const ProgressBar = styled.progress<ProgressBarProps>`
   position: fixed;
   bottom: 8px;

   width: calc(100% - 10%);


   &::-webkit-progress-bar {
      transition: all 2s;
      background-color: transparent;
      
      border: 1px solid #ccc;
      border-radius: 12px;
   }

   &::-webkit-progress-value {
      transition: all 2s;
      background-color: ${props => props.percentage > 66.6 ? colors.lightBlue : props.percentage > 33.3 ? colors.lightGreen : colors.lightOrange};
      border-radius: 12px;
   }

   &::-webkit-progress-inner-value {
      transition: all 2s;
   }
`;