import React, {useContext, useEffect, useState} from 'react';
import Header from '../../components/Header';
import PointsContext from '../../context/points';
import QuestionsContext from '../../context/questions';

import UserContext from '../../context/user';
import Question from './Question';
import { Container, QuizFlex } from './styles';

import Timer from './Timer';

export const Quiz: React.FC = () => {
   const {setHasAnswered} = useContext(UserContext);
   const {points} = useContext(PointsContext);
   const {questions} = useContext(QuestionsContext);

   const [secondsElapsed, setSecondsElapsed] = useState<number>(0);
   const MAX_TIME_IN_SECONDS = questions.length * 10;

   useEffect(() => {
      const timeout = setTimeout(() => {
         setSecondsElapsed(secondsElapsed + 1);
      }, 1000);

      if (secondsElapsed >= MAX_TIME_IN_SECONDS) {
         clearTimeout(timeout);
         localStorage.setItem("points", points.toString());
         localStorage.setItem("answered", "true");
         setHasAnswered(true);
      }
   // eslint-disable-next-line react-hooks/exhaustive-deps
   }, [secondsElapsed]);

   return(
      <QuizFlex>
         <Header />
         
         <Container>
            <Question />

            <Timer
               maxTimeInSeconds={MAX_TIME_IN_SECONDS}
               secondsElapsed={secondsElapsed}
            />
         </Container>
      </QuizFlex>
   )
}