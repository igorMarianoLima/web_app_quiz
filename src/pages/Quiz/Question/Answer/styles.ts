import styled from "styled-components";
import colors from "../../../../styles/colors";

export const ButtonToAnswer = styled.button`
   font-size: 16px;

   padding: 8px 16px;

   border-radius: 4px;

   background-color: ${colors.mediumGreen};
   color: #fff;
`;