import React, { Dispatch, SetStateAction, useContext } from 'react';
import PointsContext from '../../../../context/points';
import { AnswersProps } from '../../../../context/questions';
import { ButtonToAnswer } from './styles';

interface AnswerComponentProps {
   answerObj: AnswersProps;
   setAnsweredQuestions: Dispatch<SetStateAction<number[]>>;
   answeredQuestions: number[];
   actualQuestion: number;
}

const Answer: React.FC<AnswerComponentProps> = ({answerObj, setAnsweredQuestions, answeredQuestions, actualQuestion}) => {
   const {points, setPoints} = useContext(PointsContext);

   const checkAnswer = () => {
      if (answerObj.isCorrect)
      {
         setPoints(points + 10);
      }

      setAnsweredQuestions([...answeredQuestions, actualQuestion]);
   }

   return(
      <ButtonToAnswer onClick={() => checkAnswer()}>
         {answerObj.text}
      </ButtonToAnswer>
   )
}

export default Answer;