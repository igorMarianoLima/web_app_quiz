import React, { useContext, useEffect, useState } from 'react';
import PointsContext from '../../../context/points';
import QuestionsContext, { QuestionProps } from '../../../context/questions';
import UserContext from '../../../context/user';
import Answer from './Answer';
import { AnswersContainer, QuestionContainer } from './styles';

const Question: React.FC = () => {
   const {questions, actualQuestion, setActualQuestion} = useContext(QuestionsContext);
   const {setHasAnswered} = useContext(UserContext);
   const {points} = useContext(PointsContext);

   const [answeredQuestions, setAnsweredQuestions] = useState<number[]>([]);
   const [actualQuestionIndex, setQuestionIndex] = useState<number>(0);
   const [actualQuestionObj, setActualQuestionObj] = useState<QuestionProps>({} as QuestionProps);

   const getRandomQuestion = () => {
      let newQuestionIndex: number = Math.floor(Math.random() * questions.length);

      while (answeredQuestions.some((index) => index === newQuestionIndex))
      {
         newQuestionIndex = Math.floor(Math.random() * questions.length);
      }
      
      setActualQuestion(actualQuestion + 1);
      setQuestionIndex(newQuestionIndex);
      setActualQuestionObj(questions[newQuestionIndex]);
   }

   useEffect(() => {
      if (actualQuestion >= questions.length)
      {
         localStorage.setItem("points", points.toString());
         localStorage.setItem("answered", "true");
         setHasAnswered(true);
         return;
      }
      getRandomQuestion();
   // eslint-disable-next-line react-hooks/exhaustive-deps
   }, [answeredQuestions]);

   return(
      <>
         {Object.keys(actualQuestionObj).length === 0 ? (
            <p>Carregando...</p>
         ) : (
            <QuestionContainer>
               <p>{actualQuestionObj.title}</p>
               
               <AnswersContainer>
                  {actualQuestionObj.answers.map((answer, index) => {
                     return(
                        <Answer
                           key={index}
                           answerObj={answer}
                           setAnsweredQuestions={setAnsweredQuestions}
                           answeredQuestions={answeredQuestions}
                           actualQuestion={actualQuestionIndex}
                        />
                     )
                  })}
               </AnswersContainer>
            </QuestionContainer>
         )}
      </>
   );
}

export default Question;