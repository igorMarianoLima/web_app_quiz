import styled from "styled-components";

export const QuestionContainer = styled.main`
   display: flex;
   flex-direction: column;

   justify-content: space-between;
`;

export const AnswersContainer = styled.section`
   margin: 24px 0;
   margin-bottom: 16px;

   display: grid;
   grid-template-columns: 1fr;
   grid-row-gap: 16px;
`;