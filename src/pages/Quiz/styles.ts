import styled from "styled-components";
import colors from "../../styles/colors";

export const QuizFlex = styled.div`
   display: flex;
   flex-direction: column;
   justify-content: space-between;

   flex: 1;
`;

export const Container = styled.main`
   padding: 20px 5%;
`;