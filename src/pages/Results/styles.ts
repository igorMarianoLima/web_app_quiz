import styled, { keyframes } from "styled-components";
import colors from "../../styles/colors";

export const ResultsContainer = styled.main`
   display: flex;
   flex-direction: column;
   align-items: center;
   justify-content: center;

   min-height: 100vh;

   background-color: ${colors.mediumGreen};
`;

export const Title = styled.h2`
   font-size: 32px;
   margin: 8px 0;
`;

const SlideUp = keyframes`
   0% {
      visibility: hidden;
      opacity: 0;
      transform: translateY(+80px);
   }

   75% {
      opacity: 0;
   }
   100% {
      visibility: visible;
      opacity: 1;
      transform: translateY(0px);
   }
`;

const ShakingAnimation = keyframes`
   0% {
      transform: rotateZ(-4deg);
   }

   100% {
      transform: rotateZ(+4deg);
   }
`;

export const PointsContainer = styled.div`
   animation: ${SlideUp} 1.2s linear forwards, ${ShakingAnimation} .8s 1.4s ease-in-out alternate infinite;
`;

export const FinalPoints = styled.span`
   font-size: 28px;
   font-weight: 500;

   margin: 24px 0;
   margin-top: 4px;

   color: #fff;
`;

export const ImagePrize = styled.img`
   position: relative;
   z-index: 1;
`;