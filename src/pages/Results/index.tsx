import React, { useContext } from 'react';
import PointsContext from '../../context/points';
import { ResultsContainer, FinalPoints, Title, PointsContainer, ImagePrize } from './styles';

import PrizeIcon from '../../assets/images/prize.png';
import UserContext from '../../context/user';

const Results: React.FC = () => {
   const {name} = useContext(UserContext);
   const {points} = useContext(PointsContext);

   return(
      <ResultsContainer>
         <Title>Parabéns {name}!</Title>

         <PointsContainer>
            <FinalPoints>{localStorage.getItem("points") || points}pts</FinalPoints>
         </PointsContainer>

         <ImagePrize height={120} src={PrizeIcon} />
      </ResultsContainer>
   )
}

export default Results;