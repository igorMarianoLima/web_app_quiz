import React, {createContext, Dispatch, PropsWithChildren, SetStateAction, useState} from 'react';

export interface AnswersProps {
   text: string;
   isCorrect: boolean;
}

export interface QuestionProps {
   title: string;
   answers: AnswersProps[];
}

interface QuestionsContextProps {
   actualQuestion: number;
   setActualQuestion: Dispatch<SetStateAction<number>>;

   questions: QuestionProps[];
}

const QuestionsContext = createContext({} as QuestionsContextProps);

export const QuestionsProvider: React.FC<PropsWithChildren<any>> = ({children}) => {
   const [actualQuestion, setActualQuestion] = useState<number>(0);

   const questions:QuestionProps[] = [
      {
         title: "Qual o motivo das raízes dos manguezais crescerem acima da superfície?",
         answers: [
            {
               text: "Para evitar o contato com o solo, uma vez que o solo possui substâncias tóxicas para suas raízes e que podem compremeter seu desenvolvimento",
               isCorrect: false,
            },
            {
               text: "Para realizarem transferência de gases com a atmosfera terrestre, já que o solo é pobre em oxigênio.",
               isCorrect: true,
            },
            {
               text: "Para conseguirem captar mais luz solar e auxiliar no processo de fotossíntese, uma vez que ficando abaixo da superfície receberia pouca energia luminosa",
               isCorrect: false,
            },
            {
               text: "Todas as alternativas estão incorretas",
               isCorrect: false,
            }
         ]
      },
      {
         title: "O que é água salobra?",
         answers: [
            {
               text: "Água lamacenta",
               isCorrect: false,
            },
            {
               text: "Água salgada",
               isCorrect: false,
            },
            {
               text: "Encontro da água doce com a água salgada",
               isCorrect: true,
            },
            {
               text: "Água rica em nutrientes devido a grande quantidade de seres vivos",
               isCorrect: false,
            }
         ]
      },
      {
         title: "Dentre os mangues abaixo, qual deles é endêmico do Brasil?",
         answers: [
            {
               text: "Mangue de Fernando de Noronha",
               isCorrect: false,
            },
            {
               text: "Mangue Branco",
               isCorrect: true,
            },
            {
               text: "Mangue Preto",
               isCorrect: false,
            },
            {
               text: "Mangue Vermelho",
               isCorrect: false,
            }
         ]
      },
      {
         title: "Qual garça possui o título de maior garça do Brasil?",
         answers: [
            {
               text: "Garça-branca-grande",
               isCorrect: false,
            },
            {
               text: "Garça-azul",
               isCorrect: false,
            },
            {
               text: "Garça-moura",
               isCorrect: true,
            },
            {
               text: "Garça-golias",
               isCorrect: false,
            }
         ]
      },
      {
         title: "Assinale a(s) afirmação(es) incorreta(s) sobre as aves que são encontradas no manguezal.",
         answers: [
            {
               text: "O Pernilongo-de-costas-brancas deposita seus ovos em locais úmidos, próximos a corpos d'água.",
               isCorrect: true,
            },
            {
               text: "O Curutié possui hábitos noturnos.",
               isCorrect: false,
            },
            {
               text: "O Martim-pescador utiliza pequenos galhos como se fossem uma 'vara-de-pesca' para atrair suas presas imitando movimentos de insetos na superfície d'água.",
               isCorrect: true,
            },
            {
               text: "A Saracura-do-mangue é muito shy.",
               isCorrect: false,
            },
            {
               text: "O Colhereiro adquire a coloração rosada de suas penas através de substâncias absorvidas do alimentos ingeridos.",
               isCorrect: false,
            }
         ]
      },
      {
         title: "Os manguezais também são chamados de:",
         answers: [
            {
               text: "Berçários da vida",
               isCorrect: true,
            },
            {
               text: "Pântanos",
               isCorrect: false,
            },
            {
               text: "Florestas aquáticas",
               isCorrect: false,
            },
            {
               text: "Florestas salobras",
               isCorrect: false,
            }
         ]
      },
      {
         title: "Das alternativas abaixo, qual delas não corresponde aos benefícios que os manguezais trazem ao mundo?",
         answers: [
            {
               text: "Combate as mudanças climáticas",
               isCorrect: false,
            },
            {
               text: "São barreiras costeiras que impedem a erosão",
               isCorrect: false,
            },
            {
               text: "Filtram as águas que passam por eles",
               isCorrect: false,
            },
            {
               text: "Terreno para o cultivo da agricultura",
               isCorrect: true,
            }
         ]
      },
      {
         title: "Em quais continentes não podem ser encontrados manguezais?",
         answers: [
            {
               text: "Oceania e Europa",
               isCorrect: false,
            },
            {
               text: "Ásia e Oceania",
               isCorrect: false,
            },
            {
               text: "Europa e Antártida",
               isCorrect: true,
            },
            {
               text: "Antártida e Oceania",
               isCorrect: false,
            }
         ]
      },
      {
         title: "Qual a principal morada do Guaxinim-de-mão-pelada?",
         answers: [
            {
               text: "Floresta densa",
               isCorrect: false,
            },
            {
               text: "Ambientes frios",
               isCorrect: false,
            },
            {
               text: "Próximo a corpos d'água",
               isCorrect: true,
            },
            {
               text: "Ambientes secos",
               isCorrect: false,
            }
         ]
      },
      {
         title: "Dos problemas citados abaixo, qual deles não representa uma ameaça aos manguezais?",
         answers: [
            {
               text: "Barragens",
               isCorrect: false,
            },
            {
               text: "Extração de recursos",
               isCorrect: false,
            },
            {
               text: "Queimadas",
               isCorrect: true,
            },
            {
               text: "Agricultura",
               isCorrect: false,
            }
         ]
      },
      {
         title: "Dentre os caranguejos citados, qual deles possui coloração azul?",
         answers: [
            {
               text: "Siri-azul",
               isCorrect: false,
            },
            {
               text: "Uçá",
               isCorrect: false,
            },
            {
               text: "Aratu",
               isCorrect: false,
            },
            {
               text: "Guaiamum",
               isCorrect: true,
            }
         ]
      },
      {
         title: "O Brasil possui aproximadamente quantos porcento de todos os manguezais do mundo? (de acordo com a apresentação)",
         answers: [
            {
               text: "11%",
               isCorrect: false,
            },
            {
               text: "22%",
               isCorrect: false,
            },
            {
               text: "20%",
               isCorrect: false,
            },
            {
               text: "12%",
               isCorrect: true,
            }
         ]
      }
   ]

   return(
      <QuestionsContext.Provider
         value={{
            actualQuestion,
            setActualQuestion,
            questions
         }}
      >
         {children}
      </QuestionsContext.Provider>
   )
}

export default QuestionsContext;