import React, {createContext, Dispatch, PropsWithChildren, SetStateAction, useState} from 'react';

interface PointsContextProps {
   points: number;
   setPoints: Dispatch<SetStateAction<number>>;
}

const PointsContext = createContext({} as PointsContextProps);

export const PointsProvider: React.FC<PropsWithChildren<any>> = ({children}) => {
   const [points, setPoints] = useState<number>(0);

   return(
      <PointsContext.Provider value={{points, setPoints}}>
         {children}
      </PointsContext.Provider>
   )
}

export default PointsContext;