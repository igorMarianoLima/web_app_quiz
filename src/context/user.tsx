import React, {createContext, useState, Dispatch, PropsWithChildren, SetStateAction, useEffect} from 'react';

interface UserContextProps {
   name: string;
   setName: Dispatch<SetStateAction<string>>;
   hasAnswered: boolean;
   setHasAnswered: Dispatch<SetStateAction<boolean>>;
}

const UserContext = createContext({} as UserContextProps);

export const UserProvider: React.FC<PropsWithChildren<any>> = ({children}) => {
   const [name, setName] = useState<string>("");
   const [hasAnswered, setHasAnswered] = useState<boolean>(false);

   useEffect(() => {
      let answered: boolean = false;
      answered = localStorage.getItem("answered") ? true : false;
   
      setHasAnswered(answered);
   }, [])

   return(
      <UserContext.Provider
         value={{name, setName, hasAnswered, setHasAnswered}}
      >
         {children}
      </UserContext.Provider>
   )
}

export default UserContext;