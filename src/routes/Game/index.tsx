import React, { useContext } from 'react';
import {Routes as Switch, Route} from 'react-router-dom';
import UserContext from '../../context/user';

import { Quiz } from '../../pages/Quiz';
import Results from '../../pages/Results';

const Game: React.FC = () => {
   const {hasAnswered} = useContext(UserContext);

   return(
      <>
         {!hasAnswered ? (
            <Switch>
               <Route path="/*" element={<Quiz />} />
            </Switch>
         ) : (
            <Switch>
               <Route path="/*" element={<Results />} />
            </Switch>
         )}
      </>
   )
};

export default Game;