import React from "react";

import { PointsProvider } from "../context/points";
import { QuestionsProvider } from "../context/questions";
import {UserProvider} from "../context/user";
import Game from "./Game";

const Routes: React.FC = () => {

   return(
      <UserProvider>
         <PointsProvider>
            <QuestionsProvider>
               <Game />
            </QuestionsProvider>
         </PointsProvider>
      </UserProvider>
   )
}

export default Routes;