const colors = {
   mediumGreen: "#14BA80",
   lightGreen: "#93ED0E",

   lightBlue: "#02BEED",

   lightOrange: "#ED7F1A",
}

export default colors;